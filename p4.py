#final program


import json
from flask import Flask
import re
from flask import jsonify
from flask import request, url_for
from flask_pymongo import PyMongo
from flask.ext.api import FlaskAPI, status, exceptions
from flask_jwt_extended import JWTManager, jwt_required,\
    create_access_token, get_jwt_identity
import thread
import time
from flask_redis import Redis


# Setup the Flask-JWT-Extended extension
app = FlaskAPI(__name__)
jwt = JWTManager(app)

app.config['MONGO_URI'] = 'mongodb://localhost:27017/StudentRecord'
app.config['REDIS_HOST'] = 'localhost'
app.config['REDIS_PORT'] = 6379
app.config['REDIS_DB'] = 0
r_server = Redis(app)
mongo = PyMongo(app)

app.secret_key = 'super-secret'  # Change this!

global_token="none"

def reset_global_token(name):
    delay=500
    time.sleep(delay)
    global global_token
    global_token="reset"
    #name = request.args.get('name')
    r_server.delete(name)
    return
    
    
@app.route('/')
def home():
    token1 = request.args.get('token')
    name = request.args.get('name')
    token = r_server.get(name)
    if token == token1:
        return "Welcome To Student Record Management System..!!"
    else:   
        return "Please login! First.."
        
        
#To print record of all students
def checkFeilds(data):
    res=re.match(r'[A-Za-z]*',str(data['UserName']))
    if res==False:
        return "invalid password format"
    check=mongo.db.User.find({"UserName":str(data["UserName"])})
    if check.count()>0:
        return "username already exist"
        
    res=re.match(r'[A-Za-z]*',str(data['Password']))
    if res==False:
        return "password is not valid format"
    res=re.match(r'[A-Za-z]*',str(data['ConfirmPassword']))
    if res :
        if str(data['Password'])!=str(data['ConfirmPassword']):
            return "passwords must be same"
    return "true"
    
    
@app.route('/SignUp',methods=['POST'])
def sign_up():
    data = request.get_json()
#checking fileds
    string=checkFeilds(data)
    if string=="true":
        mongo.db.User.insert({"UserName":str(data['UserName']),"Password":str(data['Password'])})
        return "inserted"        
    else:
        return string
        
        
@app.route('/SignIn',methods=['POST'])
def sign_in():
    input_data=request.get_json()
    ip=input_data['UserName']
    pswd=input_data['Password']
    ip1 = str(ip)
    pswd1 = str(pswd)
    search=mongo.db.User.find({"$and":[{'UserName' : ip},{"Password": pswd}]})

    if search.count() > 0:
        ret = create_access_token(identity=ip)
       # global global_token
        global_token = ret
        r_server.set(ip1, ret)
        
        thread.start_new_thread(reset_global_token,(ip1,))
        if r_server.get(ip1) is None:
        	s1 = "Not present on server"
        	return s1
        else :
        	return "this "+r_server.get(ip1)+  "   ===that=====  "+ret
        #return "Logged In\n"+global_token
    else:
  	s = "Invalid username or password"
        if r_server.get(ip1) is None:
        	s = s + " And not present on server"
        return s
    
    
@app.route('/Students', methods=['GET'])
def get_all_records():
    token1 = request.args.get('token')
    name = request.args.get('name')
    token = r_server.get(name)
    if token == token1:
        data = mongo.db.Registration
        output = []
        for record in data.find():
            output.append({'First Name' : record['FirstName'],'Last Name' : record['LastName'],'Student Id' : record['StudentID'] , 'Mobile' : record['Mobile']})
        return output
    else:   
        return "Please login! First.."
        
#To get record of  specific student
@app.route('/Students/<studentId>', methods=['GET'])
def get_record_by_id(studentId):
    token1 = request.args.get('token')
    name = request.args.get('name')
    token = r_server.get(name)
    if token == token1:
        data = mongo.db.Registration
        output = []
        for record in data.find({'StudentID':studentId}):
            output.append({'First Name' : record['FirstName'],'Last Name' : record['LastName'],'Student Id' : record['StudentID'] , 'Mobile' : record['Mobile']})
        return output
    else:
        return "Please Login First"
        
        
#To delete a record
@app.route('/Students/<studentId>' ,methods=['DELETE'])
def delete_record_by_id(studentId):
    token1 = request.args.get('token')
    name = request.args.get('name')
    token = r_server.get(name)
    if token == token1:
        data = mongo.db.Registration.remove({'StudentID':studentId})
        return "Record Deleted"
    else:
        return "Please Login First"
        
        
#To add a record
@app.route('/Students',methods =['POST'])
def add_record():
    token1 = request.args.get('token')
    name = request.args.get('name')
    token = r_server.get(name)
    if token == token1:
        data = request.get_json()
        mongo.db.Registration.insert(data)
        return "Record Inserted"
    else:
        return "Please Login First"
        
        
#To update a record
@app.route('/Students/<studentId>',methods =['PUT'])
def update_record(studentId):
    token1 = request.args.get('token')
    name = request.args.get('name')
    token = r_server.get(name)
    if token == token1:
        data = request.get_json()
        mongo.db.Registration.update({'StudentID':studentId},{"$set":data},upsert=False)
        return "Record Updated"
    else:
        return "Please Login First"
        
        
@app.route('/SignOut',methods=['PUT'])
def log_out():
    global global_token
    name = request.args.get('name')
    r_server.delete(name)
    global_token="reset"
    s1 = "Logged Out Successfully"
    if r_server.get(name) is None:
        	s1 = s1+" And not present on server"
    return s1
     
 
if __name__ == "__main__":
    app.run(debug=True)
