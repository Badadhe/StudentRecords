from flask import Flask,request
from werkzeug import secure_filename
import cv2
import os
import sys
from string import Template
from PIL import Image

app = Flask(__name__)

app.config['UPLOAD_FOLDER']='/home/shruti/Desktop/Intern/image_processing/uploads'
app.config['ALLOWED_EXTENSIONS'] = set(['png', 'jpg', 'jpeg', 'gif'])

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

@app.route('/upload' ,methods=['GET','POST'])
def upload_file():
     if request.method == 'POST':
     	f = request.files['file']
     	
     	if f and allowed_file(f.filename):
			filename = secure_filename(f.filename)
			f.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
			#cv2.imwrite("/home/shruti/Desktop/Intern/image_processing/uploads/thumbnail.png", f)
			return "File uploaded successfully.."
        else :
        	return "Incorrect file format.."

@app.route('/compress',methods=['POST'])
def compress():
    f=request.files['file']
    if f and allowed_file(f.filename):
        f.save(os.path.join(app.config['UPLOAD_FOLDER'],f.filename))
        im=Image.open("/home/shruti/Desktop/Intern/image_processing/uploads/"+f.filename)
        os.remove("/home/shruti/Desktop/Intern/image_processing/uploads/"+f.filename)
        im.save("/home/shruti/Desktop/Intern/image_processing/compressed/"+"_compressed_"+f.filename,optimize=True,quality=5)
        return "Image Compressed"
    else:
        return "Invalid File"



        	
@app.route('/uploadpro' ,methods=['GET','POST'])
def upload_profile():
	f = request.files['file']
	
	if f and allowed_file(f.filename):
		f.save(os.path.join(app.config['UPLOAD_FOLDER'], f.filename))
		face_cascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

		scale_factor = 1.1
		min_neighbors = 3
		min_size = (30, 30)
		flags = cv2.cv.CV_HAAR_SCALE_IMAGE

		image = cv2.imread("/home/shruti/Desktop/Intern/image_processing/uploads/"+f.filename)

		faces = face_cascade.detectMultiScale(image, scaleFactor = scale_factor, minNeighbors = min_neighbors,
		minSize = min_size, flags = flags)

		for( x, y, w, h ) in faces:
			cv2.rectangle(image, (x, y), (x + w, y + h), (255, 255, 0), 2)
			cropped = image[y:y+h,x:x+w]
			cv2.imwrite("/home/shruti/Desktop/Intern/image_processing/profile/"+"profile_"+f.filename,cropped)
			os.remove("/home/shruti/Desktop/Intern/image_processing/uploads/"+f.filename)
			cv2.destroyAllWindows()
			return "Profile picture uploaded.."
	else:
		return "Invalid File"		
	
@app.route('/uploadthumb' ,methods=['GET','POST'])
def upload_thumbnail():


	size = 128, 128
	f = request.files['file']
	
	if f and allowed_file(f.filename):
		f.save(os.path.join(app.config['UPLOAD_FOLDER'], f.filename))
		im = Image.open("/home/shruti/Desktop/Intern/image_processing/uploads/"+f.filename)
		cropped=im.thumbnail(size, Image.ANTIALIAS)
		im.save("/home/shruti/Desktop/Intern/image_processing/thumbnail/"+"thumbnail_"+f.filename,'JPEG')
		os.remove("/home/shruti/Desktop/Intern/image_processing/uploads/"+f.filename)
		return "Thumbnail Uploaded.."
	
	else:
		return "Invalid File"	
	
	
if __name__ == '__main__':
   app.run(debug = True)
