import json
from flask import Flask
from flask_redis import Redis
from redis import Redis
from flask_redis import Redis
import re
from flask import jsonify
from flask import request, url_for
from flask_pymongo import PyMongo
from flask_api import FlaskAPI, status, exceptions
from flask_jwt_extended import JWTManager, jwt_required,\
    create_access_token, get_jwt_identity
import thread
import time
# Setup the Flask-JWT-Extended extension
app = FlaskAPI(__name__)
app.config['REDIS_URL'] = 'redis://localhost:6379/0'
redis1 = Redis(app)
jwt = JWTManager(app)
app.config['MONGO_URI'] = 'mongodb://localhost:27017/StudentRecord'
mongo = PyMongo(app)
app.secret_key = 'super-secret'  # Change this!
global_token="none"
def reset_global_token(username):
    delay=100
    time.sleep(delay)
    redis1.delete(username)
    return
@app.route('/')
def home():
    token = request.args.get('token')
    username=request.args.get('un')
    if token == redis1.get(username):
        return "Welcome To Student Record Management System..!!"
    else:   
        return "Please login! First"
#To print record of all students
def checkFeilds(data):
    res=re.match(r'[A-Za-z]*',str(data['UserName']))
    if res==False:
        return "invalid password format"
    check=mongo.db.User.find({"UserName":str(data["UserName"])})
    if check.count()>0:
        return "username already exist"
        
    res=re.match(r'[A-Za-z]*',str(data['Password']))
    if res==False:
        return "password is not valid format"
    res=re.match(r'[A-Za-z]*',str(data['ConfirmPassword']))
    if res :
        if str(data['Password'])!=str(data['ConfirmPassword']):
            return "passwords must be same"
    return "true"
@app.route('/SignUp',methods=['POST'])
def sign_up():
    data = request.get_json()
#checking fileds
    string=checkFeilds(data)
    if string=="true":
        mongo.db.User.insert({"UserName":str(data['UserName']),"Password":str(data['Password'])})
        return "inserted"        
    else:
        return string
@app.route('/SignIn',methods=['POST'])
def sign_in():
    input_data=request.get_json()
    ip=input_data['UserName']
    pswd=input_data['Password']
    search=mongo.db.User.find({"$and":[{'UserName' : ip},{"Password": pswd}]})
    if search.count() > 0:
        val=redis1.get(input_data['UserName'])
        if val is None:
            ret = create_access_token(identity=ip)
            redis1.set(ip,ret) #we use list1 as a list and push element1 as its element
            thread.start_new_thread(reset_global_token,(ip,))
            return "Logged In\n"+ret
        else:
            return "You already logged"
    else:
        return "Invalid Username or Password"
    
@app.route('/Students', methods=['GET'])
def get_all_records():
    token=request.args.get('token')
    username=request.args.get('un')
    if username is None:
        return "pass username"
    else:
        if token ==redis1.get(username):
            data = mongo.db.Registration
            output = []
            for record in data.find():
                output.append({'First Name' : record['FirstName'],'Last Name' : record['LastName'],'Student Id' : record['StudentID'] , 'Mobile' : record['Mobile']})
            return output
        else:
            return "Please Login First"
#To get record of  specific student
@app.route('/Students/<studentId>', methods=['GET'])
def get_record_by_id(studentId):
    token=request.args.get('token')
    username=request.args.get('un')
    if username is None:
        return "pass username"
    else:
        if token ==redis1.get(username):
            data = mongo.db.Registration.find({"StudentID":studentId})
            return data
        else:
            return "Please Login First"
#To delete a record
@app.route('/Students/<studentId>' ,methods=['DELETE'])
def delete_record_by_id(studentId):
    token=request.args.get('token')
    username=request.args.get('un')
    if username is None:
        return "pass username"
    else:
        if token ==redis1.get(username):
            data = mongo.db.Registration.remove({'StudentID':studentId})
            return data
        else:
            return "Please Login First"       
#To add a record
@app.route('/Students',methods =['POST'])
def add_record():
    token=request.args.get('token')
    username=request.args.get('un')
    if username is None:
        return "pass username"
    else:
        if token ==redis1.get(username):
            data = request.get_json()
            mongo.db.Registration.insert(data)
            return "Record Inserted"
        else:
            return "Please Login First"
#To update a record
@app.route('/Students/<studentId>',methods =['PUT'])
def update_record(studentId):
    token=request.args.get('token')
    username=request.args.get('un')
    if username is None:
        return "pass username"
    else:
        if token ==redis1.get(username):
            data = request.get_json()
            mongo.db.Registration.update({'StudentID':studentId},{"$set":data},upsert=False)
            return "Record Updated"
        else:
            return "Please Login First"
    
@app.route('/SignOut',methods=['GET'])
def log_out():
    username=request.args.get('un')
    redis1.delete(username)
    return "Logged Out Successfully"
    
    
    
 
if __name__ == "__main__":
    app.run(debug=True)
