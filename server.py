#To connect a database of student records and then perform insert, delete, update, print records

from flask import Flask
from flask import jsonify
from flask import request, url_for
from flask_pymongo import PyMongo
from flask_api import FlaskAPI, status, exceptions

app = FlaskAPI(__name__)

#app.config['MONGO_DBNAME'] = 'test'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/DB'

mongo = PyMongo(app)

@app.route('/')
def home():
	return "Welcome To Student Record Management System"

#To print record of all students
@app.route('/Students', methods=['GET'])
def get_all():
    record = mongo.db.contacts
    output = []
    for s in record.find():
        output.append({'Fname' : s['Fname'], 'Lname' : s['Lname'], 'ID' : s['studentID']})
    return output


#To get record of  specific student

@app.route('/Students/<studentId>', methods=['GET'])
def get_record_by_id(studentId):

	data = mongo.db.contacts
	output = []

	for record in data.find({'studentID':studentId}):
		output.append({'First Name' : record['Fname'],'Last Name' : record['Lname'],'Student Id' : record['studentID'] , 'Mobile' : record['Mobile']})

	return output



# Adding Records

@app.route('/Students',methods =['POST'])
def add():
   	note = request.get_json()
   	mongo.db.contacts.insert(note)
   	return "added your record"
   	
   	

#To delete a record

@app.route('/Students/<StudentId>' ,methods=['DELETE'])
def delete_by_id(StudentId):
    star = mongo.db.contacts.remove({'studentID':StudentId})
    return "Deleted"
    

#To update a record

@app.route('/Students/<StudentId>',methods =['PUT'])
def update_record(StudentId):

	note = request.get_json()
	mongo.db.contacts.update({'StudentID':studentId},{"$set":note},upsert=False)

	return "Record Updated"
    

if __name__ == "__main__":
    app.run(debug=True)

